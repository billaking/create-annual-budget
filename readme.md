# About this Repo

This is the Git repo of the Annual Budget Generator written in Java using the NetBeans IDE version 8.2
 
Creating a budget is really simple with this Java application. The benefit of using this application is that once you configure the xml file with your financial transactions, it generates a qif file that could be importanted into any financial application like quicken or GnuCash.

Setup the xml file with all the transactions, amounts, date of the transaction and the bank category. When the next year comes, just change the year in the xml file and then run it. In seconds you will have a new qif file for the next financial calendar year.

### [About the finances.xml file](https://gitlab.com/billaking/create-annual-budget/wikis/finance-xml)

### [About the category files](https://gitlab.com/billaking/create-annual-budget/wikis/categories)

# Packaging the Java application in NetBeans for distribution.

## Getting things setup:
### Configure the main class.

- Right-click the project's node and choose properties
Select the Run panel and enter the main class field.
Click OK to close the Project Properties dialog box.

### Add the Swing Layout Extension library to the project.

- Right-click the project's Libraries node and choose Add Library
In the Add Libraries dialog box, select Swing Layout Extension and click Add Library.
Click OK to close the Project Properties dialog box.

### Build the Project and create the Jar file
- Choose Build > Build Main Project

### Distributing the Application to Other Users
- Browse to the dist folder.
- Create a zip file that contains the application Jar file and the accompanying lib folder that contains the swing-layout-1.0.jar
- Users can unpack and double-click the jar file to run it.
