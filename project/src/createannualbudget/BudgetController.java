/**
 * Pseudo code *********
 * Start the program
 * Read XML file
 * Write finances settings
 * Read categories
 * - expenses
 * - income
 * Write categories
 * Read stocks
 * write stocks
 * Get transaction
 * While transaction exists
 * - Read line
 *  - Is it a recurring
 *   - write line
 *  - Is it a occasional
 *   - write line
 * End While
 * Write transaction to output
 * Dispose
 * End the program
 */
package createannualbudget;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author billaking
 */
public class BudgetController {

    BudgetModel budget = new BudgetModel();
    FileWriter fw = null;

    /**
     * Read XML file
     */
    public void start() {
        readAppXML();
        readXML();
        writeXML();
    }

//    private void inputReader(){
    // todo: The input reader will read the settings from settings.ini
    // and set global application settings.
//    }
//    
    /**
     * The locateXML file is located in the root of the project.
     */
    private void readXML() {
        System.out.println(budget.getTransactionsFileName());
        try {
            budget.setXMLFile(new File(buildPath(budget.getTransactionsFileName())).getAbsolutePath());
        } catch (Exception ex) {
            System.out.println("Error reading transaction file");
        }
    }

    /**
     * Read the appsettings file to get settings for the application:
     * transaction xml file: used to set transaction dates, qif file name, monthly
     * transactions, occasional transactions.
     * 
     * category files: income, expenses, stocks or securities
     */
    private void readAppXML() {
        try {            
            File f = new File("appsettings.xml");
            String appsettingspath = f.getAbsolutePath();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            
            Document doc = dBuilder.parse(appsettingspath);
            budget.setExpenseFileName(doc.getDocumentElement().getAttribute("expensesfile"));
            budget.setIncomeFileName(doc.getDocumentElement().getAttribute("incomefile"));
            budget.setSecuritiesFileName(doc.getDocumentElement().getAttribute("securitiesfile"));
            budget.setTransactionsFileName(doc.getDocumentElement().getAttribute("transactionsfile"));
            System.out.println("expenses file name: " + budget.getExpenseFileName() + "\nincome file name: " 
                    + budget.getIncomeFileName() + "\nsecurities file name: " 
                    + budget.getSecuritiesFileName() + "\ntransactions file name: " 
                    + budget.getTransactionsFileName());
        } catch (Exception ex) {
           System.out.println("Error " + ex.getMessage());
        }        
    }

    private void writeXML() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doTransactions(doc(dBuilder, budget.getXMLFile()));
        } catch (Exception ex) {
            Logger.getLogger(BudgetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String buildPath(String filename) {
        File f = new File(filename);
        if (f.exists() && !f.isDirectory()) {
            return filename;
        } else {
            return "resources/" + filename;
        }
    }

  

    /**
     * This code will read / write file and begin parsing data into the OFX
     */
    private void doTransactions(Document doc) {
        try {
            budget.setBudgetName(doc.getDocumentElement().getAttribute("budgetname"));
            budget.setBudgetYear(doc.getDocumentElement().getAttribute("budgetyear"));
            budget.setStartMonth(doc.getDocumentElement().getAttribute("startmonth"));
            budget.setEndMonth(doc.getDocumentElement().getAttribute("endmonth"));
            budget.setQifFileName(budget.getBudgetName() + budget.getBudgetYear() + ".qif");
            FileWriter fw = new FileWriter(budget.getQifFileName(), true);
            /**
             * Budget items built out from a sample from a Wells Fargo import
             */
            fw.write("");
            fw.write("");
            fw.write("!Type:Cat\n");

            fw.write(buildCategories(buildPath(budget.getExpenseFileName()), "expenses").toString());
            fw.write(buildCategories(buildPath(budget.getIncomeFileName()), "income").toString());
            fw.write("!Type:Security\n");
            fw.write(buildCategories(buildPath(budget.getSecuritiesFileName()), "securities").toString());
            fw.write("!Account\n");
            fw.write("NPrimary\n");
            fw.write("D\n");
            fw.write("TBank\n");
            fw.write("B1,468.53\n");
            fw.write("^\n");
            fw.write("!Type:Bank\n");
            //initialize();
            NodeList nList = doc.getElementsByTagName("transaction");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    budget.setTransactiondate(eElement.getAttribute("date"));
                    budget.setPayee(eElement.getAttribute("payee"));
                    budget.setCategory(eElement.getAttribute("category"));
                    budget.setAmount(eElement.getAttribute("amount"));
                    budget.setMemo(eElement.getAttribute("memo"));
                    if (budget.getTransactiondate().contains("/")) {
                        fw.write("D" + budget.getTransactiondate() + "/" + budget.getBudgetYear() + "\n");
                        fw.write("M" + budget.getMemo() + "\n");
                        fw.write("P" + budget.getPayee() + "\n");
                        fw.write("T" + budget.getAmount() + "\n");
                        fw.write("N" + "\n");
                        fw.write("L" + budget.getCategory() + "\n");
                        fw.write("^\n");
                    } else {
                        int stMonth = Integer.parseInt(budget.getStartMonth());
                        int stEnd = Integer.parseInt(budget.getEndMonth());

                        for (int i = stMonth; i <= stEnd; i++) {
                            if (budget.getTransactiondate() != null && i != 13) {
                                fw.write("D" + i + "/" + budget.getTransactiondate()
                                        + "/" + budget.getBudgetYear() + "\n");
                                fw.write("M" + budget.getMemo() + "\n");
                                fw.write("P" + budget.getPayee() + "\n");
                                fw.write("T" + budget.getAmount() + "\n");
                                fw.write("N" + "\n");
                                fw.write("L" + budget.getCategory() + "\n");
                                fw.write("^\n");
                            }
                        }
                    }
                }
            }
            fw.close();
        } catch (Exception ex) {
            Logger.getLogger(BudgetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This function takes a filename with its extension and the switch to use
     * to write the category type.
     *
     * @param file
     * @param execute
     * @return is a stringbuilder object with categories to write to the
     * filestream
     */
    private StringBuilder buildCategories(String file, String execute) {
        // income category file to open 
        StringBuilder cat = new StringBuilder();
        String textfile = new File(file).getAbsolutePath();
        // This will reference one line at a time
        String line = null;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(textfile);
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            switch (execute) {
                case "expenses":
                    while ((line = bufferedReader.readLine()) != null) {
                        cat.append("N".concat(line).concat("\n"));
                        cat.append("D".concat("\n"));
                        cat.append("E".concat("\n"));
                        cat.append("^".concat("\n"));
                    }
                    break;
                case "income":
                    while ((line = bufferedReader.readLine()) != null) {
                        cat.append("N".concat(line).concat("\n"));
                        cat.append("D".concat("\n"));
                        cat.append("I".concat("\n"));
                        cat.append("^".concat("\n"));
                    }
                    break;
                case "stocks":
                    while ((line = bufferedReader.readLine()) != null) {
                        cat.append("N".concat(line).concat("\n"));
                        cat.append("S".concat(line).concat("\n"));
                        cat.append("^".concat("\n"));
                    }
                    break;
            }

            // Always close files.
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '"
                    + textfile + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                    + textfile + "'");
            // Or we could just do this: 
            // ex.printStackTrace();
        }
        return cat;
    }

    /**
     * This method is responsible for building a document object that the code
     * can write too.
     *
     * @param d DocumentBuilder object
     * @param filename the name of the file
     * @return
     */
    private Document doc(DocumentBuilder d, String filename) {
        try {
            System.out.println(MessageFormat.format("Building {0} document",filename));
            Document c = (d.parse(filename));
            return c;
        } catch (Exception ex) {
            System.out.println(MessageFormat.format("Error building {0} document", filename));
            return null;
        }
    }
}
