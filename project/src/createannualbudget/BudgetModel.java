package createannualbudget;

/**
 *
 * @author billaking
 */
public class BudgetModel {

    private String budgetyear;
    private String monthstart;
    private String monthend;
    private String payee;
    private String amount;
    private String transactiondate;
    private String category;
    private String filelocation;
    private String xmlfile;
    private String memo;
    private String savedname;
    private String budgetname;
    private String qiffilename;
    private String expensesfilename;
    private String incomefilename;
    private String securitiesfilename;
    private String transactionsfilename;

    /**
     * @return the filePath
     */
    public String getQifFileName() {
        return qiffilename;
    }

    /**
     * @param filePath the memo to set
     */
    public void setQifFileName(String filePath) {
        this.qiffilename = filePath;
    }

    /**
     *
     * /
     *
     **
     * @return the fileName
     */
    public String getBudgetName() {
        return budgetname;
    }

    /**
     * @param fileName the memo to set
     */
    public void setBudgetName(String fileName) {
        this.budgetname = fileName;
    }

    /**
     * @return the savedName
     */
    public String getsavedName() {
        return savedname;
    }

    /**
     * @param savedName the memo to set
     */
    public void setsavedName(String savedName) {
        this.savedname = savedName;
    }

    /**
     * @return the memo
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param memo the memo to set
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return the budgetYear
     */
    public String getBudgetYear() {
        return budgetyear;
    }

    /**
     * @param budgetYear the budgetYear to set
     */
    public void setBudgetYear(String budgetYear) {
        this.budgetyear = budgetYear;
    }

    /**
     * @return the startMonth
     */
    public String getStartMonth() {
        return monthstart;
    }

    /**
     * @param startMonth the startMonth to set
     */
    public void setStartMonth(String startMonth) {
        this.monthstart = startMonth;
    }

    /**
     * @return the endMonth
     */
    public String getEndMonth() {
        return monthend;
    }

    /**
     * @param endMonth the endMonth to set
     */
    public void setEndMonth(String endMonth) {
        this.monthend = endMonth;
    }

    /**
     * @return the payee
     */
    public String getPayee() {
        return payee;
    }

    /**
     * @param payee the payee to set
     */
    public void setPayee(String payee) {
        this.payee = payee;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the transactiondate
     */
    public String getTransactiondate() {
        return transactiondate;
    }

    /**
     * @param transactiondate the transactiondate to set
     */
    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the filelocation
     */
    public String getFilelocation() {
        return filelocation;
    }

    /**
     * @param filelocation the filelocation to set
     */
    public void setFilelocation(String filelocation) {
        this.filelocation = filelocation;
    }

    /**
     * @return the xmlfile name
     */
    public String getXMLFile() {
        return xmlfile;
    }

    /**
     * @param file of the xmlfile name
     */
    public void setXMLFile(String file) {
        this.xmlfile = file;
    }

    public void setExpenseFileName(String file) {
        this.expensesfilename = file;
    }

    public String getExpenseFileName() {
        return expensesfilename;
    }

    public void setIncomeFileName(String file) {
        this.incomefilename = file;
    }

    public String getIncomeFileName() {
        return incomefilename;
    }

    public void setSecuritiesFileName(String file) {
        this.securitiesfilename = file;
    }

    public String getSecuritiesFileName() {
        return securitiesfilename;
    }

    public void setTransactionsFileName(String file)    {
        this.transactionsfilename = file;
    }

    public String getTransactionsFileName() {
        return transactionsfilename;
    }

}
