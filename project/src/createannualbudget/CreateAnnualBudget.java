/**
 * This application is used annually to create a household budget that can be
 * used daily, monthly and annually to track exactly where the family household
 * budget exists.
 * 
 * see Readme.MD for instructions on how to use
 * this program.
 */
package createannualbudget;
/**
 * Pseudo code *********
 * Start the program
 * Read XML file
 * Write finances settings
 * Read categories
 * - expenses
 * - income
 * Write categories
 * Read stocks
 * write stocks
 * Get transaction
 * While transaction exists
 * - Read line
 *  - Is it a recurring
 *   - write line
 *  - Is it a occasional
 *   - write line
 * End While
 * Write transaction to output
 * Dispose
 * End the program
 * 
 * @author billaking
 */
public class CreateAnnualBudget {

    // Initialize controller and it's methods
   BudgetController controller = new BudgetController();
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // TODO code application logic here
        new CreateAnnualBudget();
    }
    // Call the controllers start method
    public CreateAnnualBudget(){
        //controller.getDataXml();
        controller.start();
    }
}
